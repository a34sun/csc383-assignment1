function cmp_2nd(a, b) {
  return a[1] - b[1];
}

const tol = 10;
const UNDEFINED_SLOPE = 999; /* To mimic a straight line, avoiding calculation errors */

function isValidPoint(i_x, i_y) {
  return !(
    i_x > window.innerWidth + 10 ||
    i_y > window.innerHeight ||
    i_x < 0 ||
    i_y < 0 ||
    isNaN(i_x) ||
    isNaN(i_y)
  );
}

class MainController {
  constructor() {
    this.initialize();
  }

  initialize() {
    this.lineAgents = [];
    this.activeLineAgent = null;
    this.changeSound = false;

    this.drawnLines = [
      new Line(UNDEFINED_SLOPE, 0, 0, [
        [0, -1, 0],
        [2, 1, window.innerHeight],
      ]),
      new Line(0, 0, 0, [
        [0, 0, 0],
        [1, window.innerWidth, 1],
      ]),

      new Line(0, window.innerHeight, 0, [
        [2, 0, window.innerHeight],
        [3, window.innerWidth + 1, window.innerHeight + 1],
      ]),
      new Line(UNDEFINED_SLOPE, 0, -window.innerWidth, [
        [1, window.innerWidth - 1, 0],
        [3, window.innerWidth + 1, window.innerHeight],
      ]),
    ];
    this.polygon_generator = new PolygonGenerator();
  }

  reset() {
    this.initialize();
    this.nextPhase();
    this.createAgents();
  }

  nextPhase() {
    if (OPTIONS.phase < COLORS.length - 1) {
      OPTIONS.phase += 1;
    } else {
      OPTIONS.phase = 0;
    }
    this.changeSound = true;
  }

  nextAgent() {
    if (this.lineAgents.length !== 0) {
      this.activeLineAgent = this.lineAgents.pop();
    } else {
      this.activeLineAgent = null;
      this.reset();
    }
  }

  drawPastLines() {
    this.drawnLines.forEach((l) => {
      drawCustomLine(0, l.y_intercept, l.x, l.y);
    });
  }

  getIntersecting(v_currLine, cmp_line) {
    if (
      v_currLine.x >= window.innerWidth &&
      cmp_line.m == UNDEFINED_SLOPE &&
      cmp_line.x_intercept == -window.innerWidth + tol
    ) {
      return [window.innerWidth, v_currLine.y];
    }

    const c1 = v_currLine.m * v_currLine.x_intercept + v_currLine.y_intercept;
    const c2 = cmp_line.m * cmp_line.x_intercept + cmp_line.y_intercept;
    const i_x = (c1 - c2) / (cmp_line.m - v_currLine.m);

    const i_y =
      cmp_line.m * (i_x + cmp_line.x_intercept) + cmp_line.y_intercept;
    return [i_x, i_y];
  }

  onFinishActiveAgent() {
    const v_currLine = this.activeLineAgent.line;

    const thisLinePoints = [];

    let prevPoint = null;
    let old_i = null;

    try {
      this.drawnLines.forEach((cmp_line, i) => {
        const [i_x, i_y] = this.getIntersecting(v_currLine, cmp_line);
        if (!isValidPoint(i_x, i_y)) return;
        const intersectObj = {
          x: i_x,
          y: i_y,
          adj: [],
        };

        this.polygon_generator.addIntersection(intersectObj);
        const new_i = this.polygon_generator.getCurrIdx();

        thisLinePoints.push([new_i, i_x, i_y]);

        if (prevPoint) {
          intersectObj.adj.push(intersectObj);
          const oldAdj = this.polygon_generator.graph.get(old_i);
          oldAdj.push(new_i);
        }

        const [prev, next] = cmp_line.getBeforeAndAfterPoints(i_x);

        if (
          cmp_line.points[prev] === undefined ||
          cmp_line.points[next] === undefined
        )
          return;
        const beforePoint_i = cmp_line.points[prev][0];
        const afterPoint_i = cmp_line.points[next][0];

        this.polygon_generator.updateGraph(
          beforePoint_i,
          afterPoint_i,
          old_i,
          new_i
        );

        cmp_line.points.splice(prev, 0, [new_i, i_x, i_y]);
        cmp_line.points.sort(cmp_2nd);

        prevPoint = intersectObj;
        old_i = new_i;
      });
    } catch (e) {
      console.log(e);
      this.reset();
    }
    thisLinePoints.sort(cmp_2nd);
    v_currLine.points = thisLinePoints;
    this.drawnLines.push(v_currLine);
    this.polygon_generator.findPolygons();

    this.drawPastLines();
    this.nextAgent();
  }

  createAgents() {
    this.lineAgents = [];

    for (let i = 0; i < OPTIONS.agentNum; i++) {
      let a = new LineAgent(this);
      this.lineAgents.push(a);
    }

    this.activeLineAgent = this.lineAgents.pop();
  }

  updateAgents() {
    this.activeLineAgent?.update();
  }

  drawAgents() {
    this.activeLineAgent?.draw(this.p);
  }
}
