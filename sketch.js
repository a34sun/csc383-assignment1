const OPTIONS = {
  agentNum: 20,
  phase: 0
}

const mainController = new MainController()
let twinkle;
function preload() {
  twinkle = loadSound("assets/twinkle.mp3")
}

function createAgents() {
  mainController.createAgents();
}
function setup() {
  createCanvas(window.innerWidth, window.innerHeight);

  frameRate(60);

  colorMode(HSB);
  const bg = COLORS[OPTIONS.phase][0];
  background(bg[0], bg[1], bg[2]);
  createAgents();
  noStroke();
}

function draw() {
  fill(0)
  mainController.updateAgents();
  mainController.drawAgents();
}

function windowResized() {
  createAgents();
}

function mousePressed() {
  userStartAudio()
}