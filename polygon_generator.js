class PolygonGenerator {
  constructor() {
    this.initialize();
  }

  initialize() {
    this.polygons = [];
    this.intersections = [
      { x: 0, y: 0, adj: [] },
      { x: window.innerWidth, y: 0, adj: [] },
      { x: 0, y: window.innerHeight, adj: [] },
      { x: window.innerWidth, y: window.innerHeight, adj: [] },
    ];

    this.graph = new Map();
    this.graph.set(0, [1, 2]);
    this.graph.set(2, [0, 3]);
    this.graph.set(1, [0, 3]);
    this.graph.set(3, [1, 2]);
  }

  addIntersection(intersctionObj) {
    this.intersections.push(intersctionObj);
    console.log(this.intersections);
  }

  getCurrIdx() {
    return this.intersections.length - 1;
  }

  turnGraphIntoVertices() {
    const v_arr = [];
    const v_set = new Set();
    for (const [key, value] of this.graph.entries()) {
      for (var i = 0; i < value.length; i++) {
        if (v_set.has(`${key}-${value[i]}`) || v_set.has(`${value[i]}-${key}`))
          continue;
        v_arr.push([key, value[i]]);
        v_set.add(`${key}-${value[i]}`);
      }
    }
    return v_arr;
  }

  findPolygons() {
    console.log(this.graph);
    try {
      const edges = this.turnGraphIntoVertices();

      edges.forEach((i) => {
        const [i1, i2] = i;
        let v1 = this.intersections[i1],
          v2 = this.intersections[i2];
        v1.adj.push(v2);
        v2.adj.push(v1);
      });

      const cycles = extract_cycles(this.intersections);
      this.polygons = cycles;
      console.log("finding polygons", cycles, this.intersections);
      this.draw();
    } catch (e) {
      console.log(e);
      mainController.reset();
    }
  }

  updateGraph(beforePoint_i, afterPoint_i, old_i, new_i) {
    const adj1 = this.graph.get(beforePoint_i);
    const adj2 = this.graph.get(afterPoint_i);

    /* graph starts changing */
    const toR_before_i = adj2.indexOf(beforePoint_i);
    const toR_after_i = adj1.indexOf(afterPoint_i);

    adj2.splice(toR_before_i, 1);
    adj1.splice(toR_after_i, 1);

    adj1.push(new_i);
    adj2.push(new_i);

    const newArr = [beforePoint_i, afterPoint_i];
    if (old_i) newArr.push(old_i);
    this.graph.set(new_i, newArr);
  }

  draw() {
    let leaveUnfilled = this.polygons.length <= 5;
    this.polygons.forEach((polygon) => {
      if (leaveUnfilled) {
        leaveUnfilled = false;
        return;
      }
      const i = Math.floor(Math.random() * COLORS[OPTIONS.phase].length);

      const color = COLORS[OPTIONS.phase][i];
      fill(color[0] - Math.random() * 20 + 10, color[1], color[2]);

      beginShape();
      polygon.forEach((v) => {
        vertex(v.x, v.y);
      });
      endShape();
    });
  }
}
