function cmp_2nd(a, b) {
  return a[1] - b[1];
}

function findClosestLower(pairArray, target) {
  var l = 0;
  var h = pairArray.length;
  var i = l - 1;
  while (l <= h) {
    var m = (l + h) >>> 1,
      x = pairArray[m];
    var p = x - target;
    if (p <= 0) {
      i = m;
      l = m + 1;
    } else {
      h = m - 1;
    }
  }
  return i;
}

function findClosestAbove(pairArray, target) {
  var l = 0;
  var h = pairArray.length;
  var i = pairArray.length;
  while (l <= h) {
    var m = (l + h) >>> 1,
      x = pairArray[m];
    var p = x - target;
    if (p >= 0) {
      i = m;
      h = m - 1;
    } else {
      l = m + 1;
    }
  }
  return i;
}

class Line {
  constructor(m, y_intercept, x_intercept = 0, points = []) {
    this.m = m;
    this.x_intercept = x_intercept;
    this.y_intercept = y_intercept;
    this.x = 0;
    this.y = y_intercept;
    this.points = points;
  }

  getNext() {
    this.x += 8;
    this.y = this.x * this.m + this.y_intercept;
    if (
      this.y <= 0 ||
      this.y >= window.innerHeight ||
      this.x > window.innerWidth
    ) {
      if (this.x < 30) {
        mainController.nextAgent();
        return;
      }
      mainController.onFinishActiveAgent();

      twinkle.play();
    }
  }

  addPoint(point) {
    this.points.append(point);
    this.points.sort(cmd_2nd);
  }

  getBeforeAndAfterPoints(i_x) {
    const onlyX = this.points.map((x) => x[1]);
    const prev = findClosestLower(onlyX, i_x);
    const next = findClosestAbove(onlyX, i_x);

    return [prev, next];
  }
}
