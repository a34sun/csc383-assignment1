function drawCustomLine(x1, x2, x3, x4) {
  stroke(43, 23, 100);
  strokeWeight(2);
  line(x1, x2, x3, x4);
  strokeWeight(0);
}

class LineAgent {
  getSlope = (angle) => {
    return Math.tan(angle);
  };

  constructor(factory, line = false) {
    this.factory = factory;
    if (line) {
      this.line = line;
      return;
    }
    this.initialize();
  }

  initialize() {
    this.angle = Math.floor(Math.random() * 40 + 5);
    this.slope = this.getSlope(this.angle);
    this.line = new Line(
      this.slope,
      Math.floor(Math.random() * window.innerHeight)
    );
  }

  reset() {}

  update() {
    this.line.getNext();
  }

  draw() {
    drawCustomLine(
      this.line.x_intercept,
      this.line.y_intercept,
      this.line.x,
      this.line.y
    );
  }
}
